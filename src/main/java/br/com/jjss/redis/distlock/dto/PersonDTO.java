package br.com.jjss.redis.distlock.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor(staticName = "of")
public class PersonDTO implements Serializable {
    private String id;
    private String name;
    private int age;


}
