package br.com.jjss.redis.distlock.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Mensagem de erro")
public class ErroDto {

	@Schema(title = "Mensagem com o erro encontrado.", example = "Erro ao realizar a operação.")
	private String mensagem;
	
}
