package br.com.jjss.redis.distlock.controller.handler;

import br.com.jjss.redis.distlock.dto.ErroDto;
import br.com.jjss.redis.distlock.exception.BadRequestException;
import br.com.jjss.redis.distlock.exception.LockException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.net.ConnectException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestControllerAdvice
public class RestControllerExceptionHandler {

	@ExceptionHandler(ConnectException.class)
	public ResponseEntity<ErroDto> validateExceptionHandler(final ConnectException ex) {
		final ErroDto ErroDto = new ErroDto(ex.getMessage());
		return new ResponseEntity<ErroDto>(ErroDto, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(BadRequestException.class)
	@ApiResponse(responseCode = "400", description = "Requisição inválida", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErroDto.class)))
	public ResponseEntity<ErroDto> badRequestExceptionHandler(final BadRequestException ex) {
		final ErroDto ErroDto = new ErroDto(ex.getMessage());
		return new ResponseEntity<ErroDto>(ErroDto, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(LockException.class)
	@ApiResponse(responseCode = "423", description = "Recurso está bloqueado para acesso", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErroDto.class)))
	public ResponseEntity<ErroDto> lockExceptionHandler(final LockException ex) {
		final ErroDto ErroDto = new ErroDto(ex.getMessage());
		return new ResponseEntity<ErroDto>(ErroDto, HttpStatus.LOCKED);
	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	public ResponseEntity<ErroDto> validateExceptionHandler(final DataIntegrityViolationException ex) {
		final ErroDto ErroDto = new ErroDto("Erro ao persistir devido a chave duplicada.");
		return new ResponseEntity<ErroDto>(ErroDto, HttpStatus.CONFLICT);
	}
	
	@ExceptionHandler(JsonMappingException.class)
	public ResponseEntity<ErroDto> unrecognizedTokenException(final JsonMappingException e) {
		String simbolo = null;
		String mensagem = e.getOriginalMessage();
		Matcher m = Pattern.compile("'(.*?)'").matcher(mensagem);
		if(m.find()){ simbolo = m.group(1); }
		if(null != simbolo){ mensagem = String.format("Símbolo '%s' não reconhecido", simbolo); }
		final ErroDto ErroDto = new ErroDto(mensagem);
		return new ResponseEntity<ErroDto>(ErroDto, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(InvalidFormatException.class)
	public ResponseEntity<ErroDto> invalidFormatException(final InvalidFormatException e) {
		Object[] argumentos = new Object[] {e.getTargetType().getName(), e.getValue()};
		final ErroDto ErroDto = new ErroDto(String.format("Símbolo '%s' não é válido para o tipo '%s'", argumentos[1], argumentos[0]));
		return new ResponseEntity<ErroDto>(ErroDto, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(MissingServletRequestParameterException.class)
	public ResponseEntity<ErroDto> missingRequestParameterException(MissingServletRequestParameterException e) {
		final ErroDto error = new ErroDto(String.format("O parâmetro %s é obrigatório", e.getParameterName()));
		return new ResponseEntity<ErroDto>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public ResponseEntity<ErroDto> methodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e) {
		final ErroDto error = new ErroDto(String.format("O valor '%s' não é válido para o parâmetro '%s'", e.getValue(), e.getName()));
		return new ResponseEntity<ErroDto>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(HttpMediaTypeNotSupportedException.class)
	public ResponseEntity<ErroDto> httpMediaTypeSuportedException(HttpMediaTypeNotSupportedException e) {
		final ErroDto error = new ErroDto("Corpo da requisição não informado");
		return new ResponseEntity<ErroDto>(error, HttpStatus.BAD_REQUEST);
	}


	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<ErroDto> handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
		ErroDto error = null;
		if (e.getCause() != null && e.getCause() instanceof MismatchedInputException) {
			error = new ErroDto("Ocorreu um erro na conversão do corpo da requisição. Verifique os valores enviados");
		} else {
			if (e.getCause() instanceof JsonMappingException) {
				return unrecognizedTokenException((JsonMappingException) e.getCause());
			}
			error = new ErroDto("Corpo da requisição não informado");
		}
		
		return new ResponseEntity<ErroDto>(error, HttpStatus.BAD_REQUEST);
	}
}
