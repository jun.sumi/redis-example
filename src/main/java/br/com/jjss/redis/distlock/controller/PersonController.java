package br.com.jjss.redis.distlock.controller;

import br.com.jjss.redis.distlock.exception.BadRequestException;
import br.com.jjss.redis.distlock.dto.PersonDTO;
import br.com.jjss.redis.distlock.dto.RangeDTO;
import br.com.jjss.redis.distlock.exception.LockException;
import br.com.jjss.redis.distlock.service.RedisListCache;
import br.com.jjss.redis.distlock.service.RedisValueCache;
import br.com.jjss.redis.distlock.service.locker.DistributedLocker;
import br.com.jjss.redis.distlock.service.locker.LockExecutionResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/api/person")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class PersonController {
    private final RedisValueCache valueCache;
    private final RedisListCache listCache;
    private final DistributedLocker locker;
    private static final String PERSON_LOCK_KEY_TEMPLATE = "person-lock-key-%s";
    private static final int PERSON_MAX_LOCKED_TIMEOUT = 6;
    private static final int PERSON_LOCK_REQUEST_TIMEOUT = 1;

    @PostMapping
    public ResponseEntity<PersonDTO> cachePerson(@RequestBody final PersonDTO dto) {
        valueCache.cache(dto.getId(), dto);
        return ResponseEntity.ok(dto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PersonDTO> getPerson(@PathVariable final String id) {
        PersonDTO dto = (PersonDTO) valueCache.getCachedValue(id);
        if (dto == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(dto);
    }

    @PutMapping
    public void updateCachePerson(@RequestBody(required = true) final PersonDTO dto) {
        boolean exists = dto.getId() != null && !dto.getId().isBlank() && valueCache.getCachedValue(dto.getId()) != null;
        if (!exists) {
            throw new BadRequestException(String.format("Nenhum recurso foi encontrado para o id %s", dto.getId()));
        }

        String lockKey = String.format(PERSON_LOCK_KEY_TEMPLATE, dto.getId());
        Callable<PersonDTO> atualizarCacheTask = () -> {
            int sleep = 4;
            log.info("Sleeping for '{}' ms", sleep);
            TimeUnit.SECONDS.sleep(sleep);
            valueCache.cache(dto.getId(), dto);
            return dto;
        };

        LockExecutionResult<PersonDTO> result = locker.lock(lockKey, PERSON_LOCK_REQUEST_TIMEOUT, PERSON_MAX_LOCKED_TIMEOUT, atualizarCacheTask);
        if (result.hasException()) {
            if (result.getException() instanceof LockException) {
                log.error("Erro ao obter o lock para atualizacao. msg {}", result.getException().getMessage());
                throw (LockException) result.getException();
            }
            throw new BadRequestException(result.getException().getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePerson(@PathVariable final String id) {
        boolean exists = id != null && !id.isBlank() && valueCache.getCachedValue(id) != null;
        if (!exists) {
            throw new BadRequestException(String.format("Nenhum recurso foi encontrado para o id %s", id));
        }

        valueCache.deleteCachedValue(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/list/{key}")
    public void cachePersons(@PathVariable final String key, @RequestBody final List<PersonDTO> persons) {
        listCache.cachePersons(key, persons);
    }

    @GetMapping("/list/{key}")
    public List<PersonDTO> getPersonsInRange(@PathVariable final String key, @RequestBody final RangeDTO range) {
        return listCache.getPersonsInRange(key, range);
    }

    @GetMapping("/list/last/{key}")
    public PersonDTO getLastElement(@PathVariable final String key) {
        return listCache.getLastElement(key);
    }

    @DeleteMapping("/list/{key}")
    public void trim(@PathVariable final String key, @RequestBody final RangeDTO range) {
        listCache.trim(key, range);
    }
}
