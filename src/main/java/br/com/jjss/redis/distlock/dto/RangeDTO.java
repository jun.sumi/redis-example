package br.com.jjss.redis.distlock.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor(staticName = "of")
public class RangeDTO implements Serializable {

    private int from;
    private int to;

}
